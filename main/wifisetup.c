#include "wifisetup.h"

#include <Wifi.h>
#include <Arduino.h>
#include <esp_wifi.h>
#include <ESPAsyncWebServer.h>
#include <AsyncTCP.h>


//Set the Async Serv settings 
AsyncWebServer server(80);
//Set the SoftAp settings 
IPAddress local_IP(192,168,4,22);
IPAddress gateway(192,168,4,9);
IPAddress subnet(255,255,255,0);

const char* input_ssid = "ssid";
const char* input_pass = "pass";

//Set the HTML page
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <title>Connect to your wifi Network</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <style>
    html {font-family: Times New Roman; display: inline-block; text-align: center;}
    h2 {font-size: 3.0rem; color: #FF0000;}
  </style>
  </head><body>
  <h2>HTML Form to Input Data</h2> 
  <form action="/get">
    Enter a SSID: <input type="text" name="ssid">
    <br>Enter a Password : <input type="text" name="pass">
    <br><input type="submit" value="Submit">
  </form><br>
</body></html>)rawliteral";
//404
void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "404 Not found");
}
void WifiConnect (){
  //Seting up and starting Sof AP
  WiFi.softAPConfig(local_IP, gateway, subnet);
  WiFi.softAP("ESPsoftAP_01","123456789");
  Serial.print("Soft-AP IP address = ");
  Serial.println(WiFi.softAPIP());

  server.on("/", HTTP_GET, [](AsyncWebServerRequest * request) {
    request->send_P(200, "text/html", index_html);
  });

  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest * request) {
    String input_message_ssid;
    String input_message_pass;
    String input_parameter;

    if (request->hasParam(input_ssid)) {
      input_message_ssid = request->getParam(input_ssid)->value();
      input_message_pass = request->getParam(input_pass)->value();
      input_parameter = input_ssid;
    }

    else {
      input_message_ssid = "No ssid";
      input_message_pass = "No password";
      input_parameter = "none";
    }
    Serial.print("SSID : "); 
    Serial.println(input_message_ssid);
    Serial.print("PASSWORD : "); 
    Serial.println(input_message_pass);
    //Try connecting to netwotk :
    delay(1000);
    WiFi.mode(WIFI_STA);
    delay(1000);
    char buffer1[1024];
    input_message_ssid.toCharArray(buffer1, 1024);
    char buffer2[1024];
    input_message_pass.toCharArray(buffer2, 1024);
    WiFi.begin(buffer1, buffer2);
    delay(1000);
    Serial.println();
    Serial.print("IP Address: ");
    Serial.println(WiFi.localIP());
    delay(5000);
    if (WiFi.status()== WL_CONNECTED){
      Serial.println("Connected");
    }else{
      Serial.println("Connection Error");
      WiFi.softAPConfig(local_IP, gateway, subnet);
      WiFi.softAP("ESPsoftAP_01","123456789");
      Serial.print("Soft-AP IP address = ");
      Serial.println(WiFi.softAPIP());
    }
  });
  server.onNotFound(notFound);
  if (WiFi.status()== WL_CONNECTED){
    server.end();
  }else{
    server.begin();
  }
}
